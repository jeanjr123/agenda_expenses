-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 13, 2020 at 01:57 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `schedule`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `idAddress` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `street` varchar(100) DEFAULT NULL,
  `number` varchar(10) DEFAULT NULL,
  `cep` varchar(15) DEFAULT NULL,
  `district` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`idAddress`, `idUser`, `street`, `number`, `cep`, `district`, `city`, `state`) VALUES
(1, 1, 'Rua Emilio Bertoni', '2175', '14.409-108', 'Jardim Dr. Antonio Petraglia', 'Franca', 'SP'),
(7, 25, 'Rua Estêvão Leão Bourroul', '5566', '14.400-750', 'Centro', 'Franca', 'SP'),
(8, 25, 'Avenida Brasil', '500', '14.401-234', 'Vila Aparecida', 'Franca', 'SP'),
(10, 27, 'Rua José Ferreira Cunha Barbosa', '205', '14.409-107', 'Jardim Doutor Antônio Petráglia', 'Franca', 'SP'),
(11, 28, 'Rua Ana Lucia Alves da Silva Cintra', '6633', '14.412-402', 'Residencial São Jerônimo', 'Franca', 'SP'),
(13, 30, 'Rua José Ferreira Cunha Barbosa', '6633', '14.409-107', 'Jardim Doutor Antônio Petráglia', 'Franca', 'SP'),
(14, 30, 'Rua México', '3366', '14.400-100', 'Jardim Consolação', 'Franca', 'SP');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `idCategory` int(11) NOT NULL,
  `descCategory` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`idCategory`, `descCategory`) VALUES
(0, 'Importação'),
(1, 'Amigo'),
(2, 'Familia'),
(3, 'Faculdade'),
(62, 'teste');

-- --------------------------------------------------------

--
-- Table structure for table `phonenumber`
--

CREATE TABLE `phonenumber` (
  `idPhonenumber` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mobilePhone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `phonenumber`
--

INSERT INTO `phonenumber` (`idPhonenumber`, `idUser`, `phone`, `mobilePhone`) VALUES
(1, 1, '(16) 3777-5544', '(16) 99222-8512'),
(4, 1, '(16) 3562-6699', '(16) 99555-8555'),
(19, 25, '(16) 3535-3535', '(16) 99588-8888'),
(23, 27, '(16) 3535-3535', '(16) 99999-8888'),
(24, 27, '(16) 3535-2525', '(16) 99988-8877'),
(25, 28, '(33) 3333-3333', '(99) 99999-9999'),
(26, 28, '(16) 3535-8888', '(16) 98855-5222'),
(29, 30, '(16) 3588-5221', '(16) 35258-8888'),
(30, 30, '(16) 3258-8444', '(16) 99828-2888');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `idToken` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `token` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`idToken`, `idUser`, `token`) VALUES
(19, 1, 'iWezmAdLuBvFUsGXTbLrhCIrgwo7O6R5vjiw52jbx6guZ8YRnHdP8fA4Txtx');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `idCategory` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `login` varchar(100) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`idUser`, `idCategory`, `name`, `login`, `email`, `password`) VALUES
(1, 2, 'jean junior silva de sousa', 'jean', 'jeanjr.silvasousa@gmail.com', '$2y$10$/vC1UKrEJQUZLN2iM3U9re/4DQP74sXCOVXlYXe/j9zuv1/MHD4o.'),
(25, 1, 'bill gates', 'bill', 'billmicrosoft@hotmail.com', '$2y$10$Ob0p8.vRzC0xlvu.7FmhROhG..GMpsRhb7HPyNl6pwB/m8DJfVuLi'),
(27, 2, 'Linus Torvalds', 'linus', 'linustorvalds@mint.com', '$2y$10$nb7lmD2zRFHZTXhBt5Qzc.Ssl.eY.8.ehw6ffdEbZtx87YZC3SKam'),
(28, 3, 'Ada Lovelace', 'ada', 'adalovelace@gmail.com', '$2y$10$66SFJUB/vfQMcwEDBIqQXuplANU1OScacxlfbgoY7Mte61p9kp4v2'),
(30, 1, 'Tim Berners Lee', 'tim', 'berners@http.com.br', '$2y$10$VwaeVNmE0kO4hUUqwTwCr.VJZdFZIWNd8ylqa6lUIohmZAfAkgHaC');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`idAddress`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`idCategory`);

--
-- Indexes for table `phonenumber`
--
ALTER TABLE `phonenumber`
  ADD PRIMARY KEY (`idPhonenumber`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`idToken`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `idCategory` (`idCategory`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `idAddress` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `idCategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `phonenumber`
--
ALTER TABLE `phonenumber`
  MODIFY `idPhonenumber` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `idToken` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `address_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`);

--
-- Constraints for table `phonenumber`
--
ALTER TABLE `phonenumber`
  ADD CONSTRAINT `phonenumber_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`);

--
-- Constraints for table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `token_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`idCategory`) REFERENCES `category` (`idCategory`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
