<?php
session_start();
requireValidSession();

error_reporting(0);
ini_set("display_errors", 0 );


loadModel('Category');
loadModel('User');
loadModel('PhoneNumber');
loadModel('Address');

$exception = null;

$categories = Category::getAllCategories();

$userResult = [];

$phonesNumber = [];

if(count($_POST) == 0 && isset($_GET['update'])){
   
    $user = User::getOne(['idUser' => $_GET['update']]);

    $user->password = "";
                        //return array
    $userResult = $user->getValues();

    $phonesNumber = PhoneNumber::getAllPhonesOneUser($user->idUser);

    $adresses = Address::getAllAddressOneUser($user->idUser);


}else if(count($_POST) > 0){

    $user = new User($_POST);
       

    $phones = explode(",", $_POST['phones']);
    $mobilePhones = explode("," , $_POST['mobilePhones']);
    $streets = explode(",", $_POST['streets']);
    $numbers = explode(",", $_POST['numbers']);
    $ceps = explode(",", $_POST['ceps']);
    $districts = explode(",", $_POST['districts']);
    $cities = explode(",", $_POST['cities']);
    $states = explode(",", $_POST['states']);


    try{
                //validate fields and check login and password
                //if errors instances appexception with errors
      $user->insertUser();

     
      
      $i = 0;

    
       
      foreach($phones as $key => $value){
         if($phones[$i] != "" || $mobilePhones[$i] != ""){

            $phoneNumber = new PhoneNumber(
                [
                'idUser' => $user->idUser, 
                'phone' => $phones[$i],
                'mobilePhone' => $mobilePhones[$i] 
                
                ]
            );



            $phoneNumber->insertPhones();

            $i++;

         }
      }

    

       
      $j = 0;
       
      foreach($ceps as $key => $value){

        if($streets[$j] != "" || $numbers[$j] != "" || $ceps[$j] != "" || $districts[$j] != ""
        || $cities[$j] != "" || $states[$j] != ""){


         $adresses = new Address(
             [
              'idUser' => $user->idUser, 
              'street' => $streets[$j],
              'number' => $numbers[$j],
              'cep' => $ceps[$j],
              'district' => $districts[$j],
              'city' => $cities[$j],
              'state' => $states[$j]
         
               
             ]
         );

         $adresses->insertAddress();

         $j++;
      }

    }

      //redirect to home.php
      header("Location: user.php");


    }catch(AppException $e){
        //show class message appexception 
        //receive errors of appexception
        $exception = $e;
    }
}



loadTemplateView('cadUser', $_POST + $userResult + [ 'phonesNumber' => $phonesNumber,
    'categories' => $categories, 'adresses' => $adresses,  'exception' => $exception  ]);

