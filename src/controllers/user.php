<?php
session_start();
requireValidSession();

loadModel('User');
loadModel('Category');


$users = User::getAllUsers();

foreach($users as $key => $value){
  
    $category = Category::getOneCategory($value->idCategory);
    
    $value->descCategory = $category->descCategory;

    
}


loadTemplateView('user', ['users' => $users]);

