<?php

 session_start();
 requireValidSession();
 loadModel('Token');
 loadModel('User');
 loadModel('PhoneNumber');

 error_reporting(0);
 ini_set("display_errors", 0 );


 $userData = [];

 $error = [];

if(isset($_POST['startDate']) && isset($_POST['endDate'])){

 $token = Token::getTokenUser($_SESSION['user']->idUser);

 $token = $token[0]->token;


$ch = curl_init("https://api.vexpenses.com/v2/team-members");

curl_setopt_array($ch, [

    // Equivalente ao -X:
    CURLOPT_CUSTOMREQUEST => 'GET',

    // Equivalente ao -H:
    CURLOPT_HTTPHEADER => [
        "Accept: application/json",
        "Authorization: $token"
    ],

    // Permite obter o resultado
    CURLOPT_RETURNTRANSFER => 1,
]);

$resposta = json_decode(curl_exec($ch), true);


if(isset($resposta)){


    foreach($resposta['data'] as $key => $value){

       $startDate = (new DateTime( $_POST['startDate']))->format('Y-m-d H:i:s');

       $endDate = (new DateTime( $_POST['endDate']))->format('Y-m-d 23:59:59');

        
        if($value['created_at'] >= $startDate && $value['created_at'] <= $endDate){


        $users = ['idCategory' => 0 ,'name' => $value['name'] ,
        'login' => $value['name'], 'email' => $value['email'], 'password' => 123];

        $user = new User($users);

        $user->insertUserImport();

        $nameUserSession = $_SESSION['user']->name;  

        sendEmail($nameUserSession,$user->name);

        $user->phone = $value['phone1'];

        $user->mobilePhone = $value['phone2'];
        
        array_push($userData, $user);


    

        $Phones = ['idUser' => $user->idUser ,'phone' => $value['phone1'],
        'mobilePhone' => $value['phone2'] ];

        $phone = new PhoneNumber($Phones);

        $phone->insertPhones();

       
    
        }

    }

}else{

     array_push($error, 1);
}
    


curl_close($ch);

}




loadTemplateView('importUsers', ['users' => $userData, 'error' => $error]);
