<?php
session_start();
requireValidSession();

error_reporting(0);
ini_set("display_errors", 0 );


loadModel("Category");

$delete = "";

if($_GET['delete']){
 
    $users =  User::get(['idCategory' => $_GET['delete']]);

    if(count($users) > 0){

         $delete = 0;

    }else{

        Category::deleteById($_GET['delete']);

        $delete = 1;

        

    }
     

     
}

$categories = Category::getAllCategories();



loadTemplateView("category", ['categories' => $categories, 'delete' => $delete]);