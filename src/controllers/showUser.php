<?php
session_start();
requireValidSession();

error_reporting(0);
ini_set("display_errors", 0 );


loadModel('User');
loadModel('Category');
loadModel('PhoneNumber');
loadModel('Address');
loadModel('Token');


if($_GET['delete']){

    $idUser = $_GET['delete'];

    $userPhones = PhoneNumber::getAllPhonesOneUser($idUser);

    foreach($userPhones as $key => $value){
         PhoneNumber::deleteById($value->idPhonenumber);
    }

    $userAdresses = Address::getAllAddressOneUser($idUser);

    foreach($userAdresses as $key => $value){
         Address::deleteById($value->idAddress);
    }

    $userToken = Token::getTokenUser($idUser);

    foreach($userToken as $key => $value){
        Token::deleteById($value->idToken);
    }

    


    User::deleteById($idUser);

    header('Location: /user.php');

    

}



$idUser = $_GET['idUser'];

$user = User::getOneUser($idUser);

$phones = PhoneNumber::getAllPhonesOneUser($idUser);

$adresses = Address::getAllAddressOneUser($idUser);

$category = Category::getOneCategory($user->idCategory);

loadTemplateView('showUser', 
[
 'user' => $user , 
 'category' => $category,
 'phones' => $phones,
 'adresses' => $adresses
 ]
);

