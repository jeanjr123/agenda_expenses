<?php

loadModel('Login');

session_start();


$exception = null;

if(count($_POST) > 0){

    $login = new Login($_POST);

    try{
                //validate fields and check login and password
                //if errors instances appexception with errors
      $user =  $login->checkLogin();
      $_SESSION['user'] = $user;

      //redirect to home.php
      header("Location: home.php");


    }catch(AppException $e){
        //show class message appexception 
        //receive errors of appexception
        $exception = $e;
    }
}
                                           //exception show message if is not null
loadView('login', $_POST + ['exception' => $exception]);