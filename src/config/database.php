<?php

interface Database
{

    public static function getConnection();
}


class MysqlDatabase implements Database
{


    public static function getConnection()
    {

        $envPath = realpath(dirname(__FILE__) . "/../env.ini");

        $env = parse_ini_file($envPath);

        $conn = new mysqli($env['host'], $env['username'], $env['password'], $env['database']);

        if ($conn->connect_error) {
            die("Erro: " . $conn->connect_error);
        }

        return $conn;
    }


    public static function getResultFromQuery($sql)
    {

        $connection = self::getConnection();

        $result = $connection->query($sql);

        $connection->close();

        return $result;
    }


    public static function executeSql($sql)
    {

        $connection = self::getConnection();

        if (!mysqli_query($connection, $sql)) {
            throw new Exception(mysqli_error($connection));
        }

        $id = $connection->insert_id;

        $connection->close();

        return $id;
    }

  
}
