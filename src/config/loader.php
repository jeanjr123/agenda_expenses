<?php

//functions for load mvc
function loadModel($modelName){

    require_once(MODEL_PATH . "/${modelName}.php");

}

                 //name view load, parameters for view load
function loadView($viewName, $params = array()){
    
    if(count($params) > 0){
   
        foreach($params as $key => $value){
         
            if(strlen($key) > 0){
                //creating variable with name key
                ${$key} = $value;
            }
        }
    }


    require_once(VIEW_PATH . "/${viewName}.php");
}


function loadTemplateView($viewName, $params = array()){
    
    if(count($params) > 0){
   
        foreach($params as $key => $value){
         
            if(strlen($key) > 0){
                //creating variable with name key
                ${$key} = $value;
            }
        }
    }

    
    require_once(TEMPLATE_PATH . "/header.php");
    require_once(VIEW_PATH . "/${viewName}.php");
    require_once(TEMPLATE_PATH . "/menu.php");
    require_once(TEMPLATE_PATH . "/footer.php");
}





