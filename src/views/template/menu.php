	<!-- begin #sidebar -->
    <div id="sidebar" class="sidebar sidebar-transparent">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<div class="image">
							<a href="javascript:;"><img src="assets/img/user-13.jpg" alt="" /></a>
						</div>
						<div class="info">
							<?= ucwords(strtolower($_SESSION['user']->name)) ?>
							
						</div>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header"><strong style="color: white;">Menu</strong></li>
			
				
					<li class="has-sub">
					    <a href="javascript:;">
					        <b class="caret pull-right"></b>
					        <i class="fa fa-user"></i>
					        <span>Usuário</span>
					    </a>
					    <ul class="sub-menu">
							<li><a href="cadUser.php">Cadastrar Usuário </a></li>
						    <li><a href="user.php">Listar Usuários</a></li>
					    </ul>
					</li>


					<li class="has-sub">
					    <a href="javascript:;">
					        <b class="caret pull-right"></b>
					        <i class="fa fa-users"></i>
					        <span>Categoria</span>
					    </a>
					    <ul class="sub-menu">
							<li><a href="cadCategory.php">Cadastrar Categoria </a></li>
						    <li><a href="category.php">Listar Categorias</a></li>
					    </ul>
					</li>

					<li class="has-sub">
					    <a href="javascript:;">
					        <b class="caret pull-right"></b>
					        <i class="fa fa-cloud-download"></i>

					        <span>Importações</span>
					    </a>
					    <ul class="sub-menu">
							<li><a href="cadToken.php">Cadastrar Token Vexpenses </a></li>
						    <li><a href="importUsers.php">Importar Usuarios Vexpenses</a></li>
					    </ul>
					</li>

			        <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
		