<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from seantheme.com/color-admin-v3.0/admin/html/login_v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Mar 2018 17:24:33 GMT -->
<head>
	<meta charset="utf-8" />
	<title>AgendaMobile</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link href="assets/css/animate.min.css" rel="stylesheet" />
	<link href="assets/css/style.min.css" rel="stylesheet" />
	<link href="assets/css/style-responsive.min.css" rel="stylesheet" />
	<link href="assets/css/theme/default.css" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="assets/plugins/pace/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body class="pace-top">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade in"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<div class="login-cover">
	    <div class="login-cover-image"><img src="assets/img/login-bg/bg-1.jpg" data-id="login-cover-image" alt="" /></div>
	    <div class="login-cover-bg"></div>
	</div>
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
	    <!-- begin login -->
        <div class="login login-v2" data-pageload-addclass="animated fadeIn">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <img src="assets/img/logo/phone.png" style="width:50px; height:50px"/> Agenda Mobile
                    <small>gerencie seus contatos aqui e tenha mais seguranca!</small>
                </div>
          
            </div>
            <!-- end brand -->
            <div class="login-content">

                 <?php include(TEMPLATE_PATH . '/messages.php');  ?>
                <form action="#" method="post" class="margin-bottom-0" >

                <div class="form-group m-b-20">
                    <label for="login">Login</label>

                    <input type="text" name="login" id="login" 
                    value="<?= isset($login) ? $login :  null ?>" 
                    class="form-control input-lg" 
                    class="<?= $errors['login'] ? 'is-invalid' : '' ?>"
                    placeholder="login" autofocus>

                    <!-- bootstrap class for show errors -->
                    <div class="invalid-feedback">
                       <strong><?= isset($errors['login']) ? " * ". $errors['login'] : '' ?></strong>  
                    </div>
                </div>

             
                
                <div class="form-group m-b-20">
                    <label>Senha</label>

                    <input type="password" name="password" id="password" 
                    class="form-control input-lg"
                    class="<?= $errors['password'] ? 'is-invalid' : null  ?>"
                    placeholder="senha" autofocus>
                    <!-- bootstrap class for show errors -->
                    <div class="invalid-feedback">
                        
                       <strong><?= isset($errors['password']) ? " * ". $errors['password'] : '' ?></strong>
                    </div>
                </div>
                    
                   
                    <div class="checkbox m-b-20">
                        <label>
                            <input type="checkbox" /> Lembrar senha
                        </label>
                    </div>
                    <div class="login-buttons">
                        <button type="submit" class="btn btn-primary btn-block btn-lg">Entrar</button>
                    </div>
                    <div class="m-t-20">
                      Ainda não é membro? Clique <a href="#">aqui</a> para cadastrar.
                    </div>
                </form>
            </div>
        </div>
        <!-- end login -->
      
        
      
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="assets/js/login-v2.demo.min.js"></script>
	<script src="assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->

	<script>
		$(document).ready(function() {
			App.init();
			LoginV2.init();
		});
	</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53034621-1', 'auto');
  ga('send', 'pageview');

</script>
</body>

<!-- Mirrored from seantheme.com/color-admin-v3.0/admin/html/login_v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Mar 2018 17:24:43 GMT -->
</html>
