<!-- #################################  PHONE MODAL ################################# -->

<div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <h4 class="modal-title" id="exampleModalLabeltel">Cadastro de Telefones</h4>
            </div>
            <div class="modal-body">
                <form id="cadTelefones" method="POST" action="cadUser.php">
                    <fieldset id="fieldPhones">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="phone">Telefone</label>
                                <input type="text" class="form-control" id="phone" name="phone" />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="mobilePhone">Celular</label>
                                <input type="text" class="form-control" id="mobilePhone" name="mobilePhone" />
                            </div>
                        </div>
                    </fieldset>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Telefone Cadastrado</th>
                                    <th scope="col">Celular Cadastrado</th>
                                    <th scope="col">Ação</th>
                                </tr>
                            </thead>
                            <tbody id="showPhones">

                                <!-- if edit user -->
                                <?php
                                if ($phonesNumber != "") {
                                    foreach ($phonesNumber as $key => $value) :
                                ?>
                                        <tr id="<?= $value->idPhonenumber ?>">
                                            <td>
                                                <input id="phone<?= $value->idPhonenumber ?>" name="phone" class="form-control" type="text" value="<?= $value->phone ?>">
                                            </td>

                                            <td>
                                                <input id="mobile<?= $value->idPhonenumber ?>" name="mobilePhone" class="form-control" type="text" value="<?= $value->mobilePhone ?>">
                                            </td>
                                            <td>

                                                <buttom class="btn btn-danger" onclick="deletePhones(<?= $value->idPhonenumber ?>)"><i class="fa fa-trash"></i></buttom>
                                                <buttom class="btn btn-warning" onclick="updatePhones('<?= $value->idPhonenumber  ?>','<?= $_GET['update'] ?>','<?= 'phone' . $value->idPhonenumber ?>', '<?= 'mobile' . $value->idPhonenumber ?>')">Atualizar</buttom>
                                            </td>
                                        </tr>

                                <?php
                                    endforeach;
                                }

                                ?>

                            </tbody>
                        </table>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="insertPhones">Cadastrar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

            </form>
        </div>
    </div>
</div>

<!-- ############################### ADDRESS MODAL ################################### -->

<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <h4 class="modal-title" id="exampleModalLabel">Cadastro de Endereço</h4>

            </div>
            <div class="modal-body">
                <form id="cadTelefones" method="POST" action="#">
                    <fieldset>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="cep">Cep</label>
                                <input type="text" class="form-control" id="cep" name="cep" required />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="street">Rua</label>
                                <input type="text" class="form-control" id="street" name="street" required />
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="number">Numero</label>
                                <input type="text" class="form-control" id="number" name="number" required />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="district">Bairro</label>
                                <input type="text" class="form-control" id="district" name="district" required />
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="city">Cidade</label>
                                <input type="text" class="form-control" id="city" name="city" required />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="state">Estado</label>
                                <input type="text" class="form-control" id="state" name="state" required />
                            </div>
                        </div>

                    </fieldset>

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Rua Cadastrada </th>
                                    <th scope="col">Numero Cadastrado</th>
                                    <th scope="col">Bairro Cadastrado</th>
                                    <th scope="col">Cep Cadastrado</th>
                                    <th scope="col">Cidade Cadastrada</th>
                                    <th scope="col" style="width: 8%">Estado Cadastrado</th>
                                    <th scope="col" style="width: 18%">Ação</th>
                                </tr>
                            </thead>
                            <tbody id="showAdresses">

                                <!-- if edit user -->
                                <?php
                                if ($phonesNumber != "") {

                                    $i = 0;

                                    foreach ($adresses as $key => $value) :
                                        $i++;
                                ?>
                                        <?php $idAddressRow = $value->idAddress ?>
                                        <?php $idAddressRow .= "address" ?>

                                        <tr id="<?= $idAddressRow ?>">
                                            <td>
                                                <input id="street<?= $i ?>" name="street" class="form-control" type="text" value="<?= $value->street ?>">

                                            </td>

                                            <td>
                                                <input id="number<?= $i ?>" name="number" class="form-control" type="text" value="<?= $value->number ?>">

                                            </td>
                                            <td>
                                                <input id="district<?= $i ?>" name="district" class="form-control" type="text" value="<?= $value->district ?>">

                                            </td>

                                            <td>
                                                <input id="cep<?= $i ?>" name="cep" class="form-control" type="text" value="<?= $value->cep ?>">

                                            </td>
                                            <td>
                                                <input id="city<?= $i ?>" name="city" class="form-control" type="text" value="<?= $value->city ?>">

                                            </td>

                                            <td>
                                                <input id="state<?= $i ?>" name="state" class="form-control" type="text" value="<?= $value->state ?>">

                                            </td>

                                            <td>

                                                <buttom class="btn btn-danger" onclick="deleteAdresses('<?= $value->idAddress ?>','<?= $idAddressRow ?>')"><i class="fa fa-trash"></i></buttom>

                                                <buttom class="btn btn-warning" onclick="updateAdresses('<?= $value->idAddress  ?>','<?= $_GET['update'] ?>','<?= 'street' . $i ?>','<?= 'number' . $i ?>','<?= 'district' . $i ?>','<?= 'cep' . $i ?>','<?= 'city' . $i ?>','<?= 'state' . $i ?>')">Atualizar
                                                </buttom>


                                            </td>
                                        </tr>

                                <?php
                                    endforeach;
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="insertAddress">Cadastrar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

            </form>
        </div>
    </div>
</div>
<!-- ########################################### END MODALS ############################ -->