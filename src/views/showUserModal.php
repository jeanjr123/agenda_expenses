
<!-- #################################  PHONE MODAL ################################# -->

<div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <h4 class="modal-title" id="exampleModalLabel">Contatos Usuário</h4>

            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Telefone</th>
                                <th scope="col">Celular</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($phones as $key => $value) : ?>
                                <tr>
                                    <td><?= $value->phone  ?></td>
                                    <td><?= $value->mobilePhone ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- ############################### ADDRESS MODAL ################################### -->

<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Enderecos</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Rua</th>
                                <th scope="col">Numero</th>
                                <th scope="col">Cep</th>
                                <th scope="col">Bairro</th>
                                <th scope="col">Cidade</th>
                                <th scope="col">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? foreach ($adresses as $key => $value) : ?>
                                <tr>
                                    <td><?= ucwords(strtolower($value->street)) ?></td>
                                    <td><?= $value->number ?></td>
                                    <td><?= $value->cep ?></td>
                                    <td><?= ucwords(strtolower($value->district)) ?></td>
                                    <td><?= ucwords(strtolower($value->city)) ?></td>
                                    <td><?= strtoupper($value->state) ?></td>
                                </tr>
                            <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>