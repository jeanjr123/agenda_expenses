<script src="assets/js/appAgendaMobile.js"></script>
<!-- begin #content -->
<div id="content" class="content">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
            </div>
            <h4 class="panel-title"> Categorias  </h4>
            
        </div>
        <div class="panel-body">
        <a type="button" class="btn btn-primary" style="margin-bottom:10px" href="cadCategory.php" >
        <i class="fa fa-plus"></i> Adicionar
        </a>

         <div class="table-responsive">
            <table id="data-table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Descrição Categorias</th>
                        <th>Açoes</th>               
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($categories as $key => $value) : ?>
                        <tr class="odd gradeX">
                            <td><?= ucwords(strtolower($value->descCategory))  ?></td>
                            <td>

                            <a type="buton" class="btn btn-danger" id="deleteCategory"
                                  onclick="deleteCategory('<?= $value->idCategory ?>')">
                                 <i class="fa fa-trash"></i> Excluir
                             </a>

                             <a type="buton" class="btn btn-warning" 
                              href="cadCategory.php?update=<?= $value->idCategory ?>">
                                 <i class="fa fa-pencil"></i> Editar
                             </a>

                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>

       
        </div>
    </div>

</div>

<?php if($delete == 0 && $_GET['delete']){ ?>
<script>
     swal("Não é possível excluir !", "Categoria possui cliente cadastrado", "error");

</script>
<?php } ?>




