
<!-- begin #content -->
<div id="content" class="content">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
            </div>
            <h4 class="panel-title">Cadastro de Categorias</h4>

        </div>
        <div class="panel-body">

            <form action="#" method="POST">
                <fieldset>
                    <?php include(TEMPLATE_PATH . '/messages.php');   ?>

                    <?php if (isset($_GET['update'])) {   ?>
                        <input type="hidden" id="idCategory" name="idCategory" 
                        value="<?= $_GET['update']   ?>">
                    <?php } ?>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="descCategory">Descrição Categoria</label>

                            <input type="text" class="form-control" id="descCategory" name="descCategory" placeholder="Insira a descrição da categoria" value="<?= $descCategory ?>" />

                        </div>
                    </div>

                </fieldset>

                <div class="row">
                    <div class="col-md-6">
                        <?php if($_GET['update']){ ?>
                            <button type="submit" id="updateCategory" class="btn btn-sm btn-success">Atualizar</button>
                        <?php }else{ ?>
                            <button type="submit" id="createCategory" class="btn btn-sm btn-success">Cadastrar</button>
                        <?php } ?>                    
                    </div>
                </div>
            </form>


        </div>
    </div>

</div>


<script>
    $(document).on("click", "#updateCategory", function() {

        
        descCategory = $('#descCategory').val();

        idCategory = $('#idCategory').val();

       

        if (descCategory == "") {

            swal("Insira a descrição da categoria!", "", "error");

        } else {
            $.ajax({

                url: "cadCategory.php",
                type: "POST",
                datatype: "json",
                data:  "idCategory="  + idCategory + "&descCategory=" + descCategory,

                success: function() {

                    $('#descCategory').val("");

                    swal("Categoria atulizada com sucesso!",
                     "", "success"
                    );

                    setTimeout(function() {
                        window.location.href = "/category.php";
                    }, 2000);
                }
            });
        }

        return false;

    });
</script>

<script>
    $(document).on("click", "#createCategory", function() {

        
        descCategory = $('#descCategory').val();

       
        

        if (descCategory == "") {

            swal("Insira a descrição da categoria!", "", "error");

        } else {
            $.ajax({

                url: "cadCategory.php",
                type: "POST",
                datatype: "json",
                data:  "descCategory=" + descCategory,

                success: function() {

                    $('#descCategory').val("");

                    swal("Categoria cadastrada com sucesso!",
                     "", "success"
                    );

                    setTimeout(function() {
                        window.location.href = "/category.php";
                    }, 2000);
                }
            });
        }

        return false;

    });
</script>