<script src="assets/js/appAgendaMobile.js"></script>
<?php require_once('cadUserModals.php'); ?>

<div id="content" class="content">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
            </div>
            <h4 class="panel-title">Cadastro de Usuario</h4>

        </div>
        <div class="panel-body">

            <form action="cadUser.php" method="POST">
                <fieldset>
                    <?php include(TEMPLATE_PATH . '/messages.php');   ?>

                    <?php if (isset($_GET['update'])) {   ?>
                        <input type="hidden" name="idUser" value="<?= $_GET['update'] ?>">
                    <?php } ?>

                    <div class="row">

                        <div class="form-group col-md-6">
                            <label for="name">Nome</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Insira o nome" <?= $errors['nomeUsuario'] ? 'is-invalid' : '' ?> value="<?= $name ?>" />
                            <div class="invalid-feedback" style="color:red">
                                <?= $errors['name'] ?>
                            </div>
                        </div>



                        <div class="form-group col-md-6">
                            <label for="login">Login</label>
                            <input type="text" class="form-control" id="login" name="login" placeholder="Insira o login" <?= $errors['login'] ? 'is-invalid' : '' ?> value="<?= $login ?>" />
                            <div class="invalid-feedback" style="color: red">
                                <?= $errors['login'] ?>
                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Insira o e-mail" <?= $errors['email'] ? 'is-invalid' : '' ?> value="<?= $email ?>" />
                            <div class="invalid-feedback" style="color: red">
                                <?= $errors['email'] ?>
                            </div>
                        </div>


                        <div class="form-group col-md-3">
                            <br>
                            <button type="button" class="btn btn-primary" style="width:100%" name="telefones" id="createPhone" data-toggle="modal" data-target="#phoneModal">
                                <i class="fa fa-phone "></i> Cadastrar Telefones
                            </button>
                        </div>


                        <div class="form-group col-md-3">
                            <br>
                            <button type="button" class="btn btn-warning" style="width:100%" data-toggle="modal" data-target="#addressModal">
                                <i class="fa fa-home "></i> Cadastrar Enderecos
                            </button>
                            <div class="invalid-feedback" style="color: red">
                                <?= $errors['createPhone'] ?>
                            </div>
                        </div>

                        <!-- inputs hidden for submit phones -->
                        <input type="hidden" id="phoneSubmit" name="phones" value="">

                        <input type="hidden" id="mobileSubmit" name="mobilePhones" value="">



                        <!-- inputs hidden for submit adresses -->
                        <input type="hidden" id="streetSubmit" name="streets" value="">

                        <input type="hidden" id="numberSubmit" name="numbers" value="">

                        <input type="hidden" id="cepSubmit" name="ceps" value="">

                        <input type="hidden" id="districtSubmit" name="districts" value="">

                        <input type="hidden" id="citySubmit" name="cities" value="">

                        <input type="hidden" id="stateSubmit" name="states" value="">



                    </div>

                    <div class="row">

                        <div class="form-group col-md-6">
                            <label for="idCategory">Categoria</label>
                            <select class="form-control" id="idCategory" name="idCategory">
                                <option value="">Selecione uma categoria..</option>
                                <?php foreach ($categories as $key => $value) : ?>
                                    <option value="<?= $value->idCategory ?>">
                                        <?= $value->descCategory ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback" style="color: red">
                                <?= $errors['idCategory'] ?>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="senha">Senha</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Insira a senha" <?= $errors['password'] ? 'is-invalid' : '' ?> value="<?= $password ?>" />
                            <div class="invalid-feedback" style="color: red">
                                <?= $errors['password'] ?>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="password_confirm">Confirmar Senha</label>
                            <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirme a senha" <?= $errors['password_confirm'] ? 'is-invalid' : '' ?> value="<?= $password_confirm  ?>" />
                            <div class="invalid-feedback" style="color: red">
                                <?= $errors['password_confirm'] ?>
                            </div>
                        </div>

                        <div>
                            <div id="phones">

                            </div>

                </fieldset>
                <div class="col-md-10">
                    <button type="submit" id="createUser" class="btn btn-sm btn-success">Cadastrar</button>
                </div>
            </form>


        </div>
    </div>

</div>


<?php  if(!$_GET['update']): ?>

<script>

//Phones Validation
$(document).on("click", "#createUser" , function(){
 
 let phone = $("#phoneSubmit").val();
 let mobilePhone = $("#mobileSubmit").val();

 if(phone == "" && mobilePhone == "" ){

       swal( "Cadastre pelo menos um telefone!" ,  "" ,  "error" );


     return false;
 }
});


//Address Validation
$(document).on("click", "#createUser" , function(){
 
 let street = $("#streetSubmit").val();
 let cep = $("#cepSubmit").val();
 let district = $("#districtSubmit").val();
 let number = $("#numberSubmit").val();
 let city = $("#citySubmit").val();
 let state = $("#stateSubmit").val();

 if(street == "" && cep == "" && district == "" && number == "" && city == "" && state == ""){
     
    swal( "Cadastre pelo menos um endereço!" ,  "" ,  "error" );

    return false;
 }

});

</script>

<?php endif  ?>

