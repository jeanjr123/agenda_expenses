<div id="content" class="content">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
            </div>
            <img src="assets/img/logo/logo-horizontal.png" style="width:150px; height:35px">
            <h4 class="panel-title"><strong>Importação</strong></h4>

        </div>
        <div class="panel-body">


            <form action="#" method="POST">
                <fieldset>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="descCategory">Data Inicio</label>

                            <input style="font-weight: bold;" type="date" class="form-control" id="startDate" name="startDate"
                            required oninvalid="this.setCustomValidity('Insira a data de inicio')"
							onchange="try{setCustomValidity('')}catch(e){}"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="descCategory">Data Fim</label>
                            <input style="font-weight: bold;" type="date" class="form-control" id="endDate" name="endDate"
                            required oninvalid="this.setCustomValidity('Insira a data Fim')"
							onchange="try{setCustomValidity('')}catch(e){}"/>
                        </div>
                    </div>
                </fieldset>


                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-sm btn-warning">Importar</button>
                    </div>
                </div>
            </form>

        </div>
</div>

        <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
            </div>
            
            <h4 class="panel-title"><strong>Usuários Importados</strong></h4>

        </div>
        <div class="panel-body">

            <div class="table-responsive">

                <table id="data-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Telefone</th>
                            <th>Celular</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($users) > 0) {
                            foreach ($users as $key => $value) { ?>
                                <tr>
                                    <td class="odd gradeX"><?= $value->name ?></td>
                                    <td class="odd gradeX"><?= $value->email ?></td>
                                    <td class="odd gradeX"><?= $value->phone ?></td>
                                    <td class="odd gradeX"><?= $value->mobilePhone ?></td>
                                </tr>
                        <?php }
                        } ?>

                    </tbody>
                </table>
            </div>


        </div>
 
        
<?php if($error[0] == 1): ?>
  <script>
    swal("Requisicao invalida!","Por favor verifique o token","error");
    
  </script>

<?php endif; ?>