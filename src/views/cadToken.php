<script src="assets/js/appAgendaMobile.js"></script>
<div id="content" class="content">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
            </div>
            <img src="assets/img/logo/logo-horizontal.png" style="width:150px; height:35px">
            <h4 class="panel-title">Token de integracao API Vexpenses</h4>

        </div>
        <div class="panel-body">

            <form action="#" method="POST">
                <fieldset>

                    <input type="hidden" id="idToken" name="idToken" value="<?= $idToken != [] ? $idToken : "" ?>">

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="descCategory">Token Público</label>

                            <input style="font-weight: bold;" type="text" class="form-control" id="token" name="token" placeholder="Insira o token publico da Api vexpenses" value="<?= $token != [] ? $token : "" ?>" />

                        </div>
                    </div>

                </fieldset>

                <div class="row">
                    <div class="col-md-6">
                        <?php if ($token != []) { ?>
                            <button type="submit" id="updateToken" class="btn btn-sm btn-warning">Atualizar</button>
                        <?php } else { ?>
                            <button type="submit" id="createToken" class="btn btn-sm btn-success">Cadastrar</button>
                        <?php } ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>