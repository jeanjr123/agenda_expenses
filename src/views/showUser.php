<script src="assets/js/appAgendaMobile.js"></script>
<?php require_once("showUserModal.php"); ?>
<!-- begin #content -->
<div id="content" class="content">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
            </div>
            <h4 class="panel-title"> Usuarios </h4>

        </div>
        <div class="panel-body">

            <a type="button" class="btn btn-primary" style="margin-bottom:10px" href="cadUser.php">
                <i class="fa fa-plus"></i> Adicionar
            </a>
            <div class="table-responsive">
                <table class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Login</th>
                            <th>Email</th>
                            <th>Categoria</th>
                            <th>Telefones</th>
                            <th>Enderecos</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="odd gradeX">
                            <td><?= ucwords(strtolower($user->name))  ?></td>
                            <td><?= $user->login ?></td>
                            <td><?= $user->email ?></td>
                            <td><?= ucwords(strtolower($category->descCategory)) ?></td>
                            <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#phoneModal">
                                    <i class="fa fa-phone "></i>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#addressModal">
                                    <i class="fa fa-home "></i>
                                </button>
                            </td>
                            <td>
                                <a type="button" href="#" class="btn btn-danger" onclick="deleteUser('<?= $_GET['idUser'] ?>')">
                                    <i class="fa fa-trash" aria-hidden="true"></i> Excluir</a>

                                <a type="button" href="cadUser.php?update=<?= $_GET['idUser'] ?>" class="btn btn-warning">
                                    <i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>

                                <a type="button" href="user.php" class="btn btn-primary">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>