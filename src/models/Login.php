<?php

loadModel('User');

class Login extends Model {
    
    public function validate(){
        $errors = [];

        //VALIDATION FIELDS BACKEND 

        if(!$this->login){
            $errors['login'] = "Login é um campo obrigatorio!";
        }

        if(!$this->password){
            $errors['password'] = "Informe a senha!";
        }

   

        if(count($errors) > 0 ){
            throw new ValidationException($errors);
        }
    }





    public function checkLogin(){


        $this->validate();

                                       //value of login set at instance 
        $user = User::getOne(['login' => $this->login]);

        if($user){
                 //password form                  //password instance
            if(password_verify($this->password, $user->password)){
                return $user;
            }
        }

        throw new AppException('Usuario ou Senha invalidos!');
    }


}
