<?php  



class User extends Model {


    protected static $tableName = "user";
    protected static $columns = [
        'idUser',
        'idCategory',
        'name',
        'login', 
        'email',
        'password',
      
    ];

    //VALIDATION FIELDS BACKEND 

    public function validate(){
        $errors = [];

        

        if(!$this->name){
            $errors['name'] = "Nome é um campo obrigatorio!";
        }

        if(!$this->login){
            $errors['login'] = "Login é um campo obrigatorio!";
        }


        if(!$this->email){
            $errors['email'] = "Email é um campo obrigatorio!";
        }

        if(!$this->password){
            $errors['password'] = "Senha é um campo obrigatorio!";
        }

        if(!$this->password_confirm){
            $errors['password_confirm'] = "Senha de Confirmacao é um campo obrigatorio!";
        }else if($this->password_confirm != $this->password){
            $errors['password_confirm'] = "Senhas devem ser iguais!";
        } 

        if(!$this->idCategory || $this->idCategory == "" ){
            $errors['idCategory'] = "Categoria é um campo obrigatorio!";
        }

      




   

        if(count($errors) > 0 ){
            throw new ValidationException($errors);
        }
    }


    public function insertUser(){
        if($this->idUser){
            
            $this->password = password_hash($this->password, PASSWORD_DEFAULT);
            $this->update($_POST['idUser']);

        }else{

        self::validate();

        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        $this->insert();
         
        $nameUserSession = $_SESSION['user']->name;  

        sendEmail($nameUserSession,$this->name);



        }
    }

    public function insertUserImport(){

        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        $this->insert();

    }



    public function getAllUsers($where = [],$columns = " * "){

        $usuarios = self::get($where ,$columns);
  
        return $usuarios;
  
  
    }


    public function getOneUser($idUser){
        $user = self::getOne(['idUser' => $idUser]);
        return $user;
    }

    

    






}