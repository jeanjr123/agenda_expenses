<?php

 class Category extends Model {

    protected static $tableName = "category";
    protected static $columns = [
      'idCategory',
      'descCategory'
    ];


    public function getOneCategory($idCategory){
        $category = self::getOne(['idCategory' => $idCategory]);
        return $category;

    }


    public function getAllCategories($where = [], $columns = " * " ){
       $categories = self::get($where, $columns);
       return $categories;
    }


    public function insertCategory(){

      if($this->idCategory){
        
         $this->update($_POST['idCategory']);

     }else{
         
         $this->insert();

     }
    }





 }