<?php

class PhoneNumber extends Model {

     protected static $tableName = "phonenumber";
     protected static $columns = [
      'idPhonenumber',
      'idUser',
      'phone',
      'mobilePhone'
     ];



     public function getAllPhonesOneUser($idUser){
         $phones = self::get(['idUser' => $idUser]);
         return $phones;
     }

     public function insertPhones(){
        $this->insert();
     }

     public function updatePhoneNumber(){
         $this->update($_POST['idPhonenumber']);
     }





}