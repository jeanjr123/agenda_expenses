<?php

class Address extends Model {

    protected static $tableName = "address";
    protected static $columns = [
      'idAddress',
      'idUser',
      'street',
      'number',
      'cep',
      'district',
      'city',
      'state'
    ];

    

    public function getAllAddressOneUser($idUser){
        $adresses = self::get(['idUser' => $idUser]);
        return $adresses;
    }

    public function insertAddress(){
      $this->insert();
   }

   public function updateAddress(){
    $this->update($_POST['idAddress']);
   }



    









}