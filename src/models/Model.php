<?php


class Model
{

    protected static $tableName = "";
    protected static $columns = [];
    protected $values = [];

    //array assoc
    function __construct($arr)
    {
        $this->loadFromArray($arr);
    }


    public function loadFromArray($arr)
    {
        if ($arr) {
            foreach ($arr as $key => $value) {
                $this->$key = $value;
            }
        }
    }


    //magic method get
    //for example: user->name acess name user
    public function __get($key)
    {

        return $this->values[$key];
    }


    public function __set($key, $value)
    {
        $this->values[$key] = $value;
    }

    public function getValues()
    {
        return $this->values;
    }



    //instances only one  GET ONE
    public static function getOne($filters = [], $columns = ' * ')
    {
        $class = get_called_class();

        $result = static::getResultFromSelect($filters, $columns);

        //if is set result instance class
        return $result ? new $class($result->fetch_assoc()) : null;
    }


    //instances all  GET ALL
    public static function get($filters = [], $columns = ' * ')
    {
        $objects = [];
        $result = static::getResultFromSelect($filters, $columns);

        if ($result) {
            //get class name
            $class = get_called_class();
            while ($row = $result->fetch_assoc()) {
                //instances object inserted in array
                array_push($objects, new $class($row));
            }
        }

        return $objects;
    }


    //INSERT
    public function insert()
    {
        //static value tableName
        $sql = "INSERT INTO " . strtolower(static::$tableName) . " ("
            //value columns class
            . implode(",", static::$columns) . ") VALUES (null ";

        foreach (static::$columns as $col) {

            //this->col = values instance columns
            $sql .= static::getFormatedValue($this->$col) . ",";
        }

        $sql[strlen($sql) - 1] = ')';



        $id = MysqlDatabase::executeSql($sql);

        $tableName = ucfirst(static::$tableName);

        $idTable = "id{$tableName}";

        //seto o id na instancia atual
        $this->$idTable = $id;
    }


    //UPDATE
    public function update($id)
    {
        //attr static of instance
        $sql = "UPDATE " . strtolower(static::$tableName) . " SET ";

        foreach (static::$columns as $col) {

            
                //col foreach                  //value column in instance
                $sql .= " ${col} = " . static::getFormatedValue($this->$col) . ",";
            
        }

        $sql[strlen($sql) - 1] = " ";


        $sql .= "WHERE id" . ucfirst(strtolower(static::$tableName)) . " = " . $id;

        

        MysqlDatabase::executeSQL($sql);
    }



    //DELETE
    public function deleteByID($id)
    {
        $sql = "DELETE FROM " . strtolower(static::$tableName) .
            " WHERE id" . static::$tableName . " = " . $id;
      

        MysqlDatabase::executeSQL($sql);
    }





    //where
    public static function getResultFromSelect($filters = [], $columns = ' * ')
    {

        $sql = "SELECT ${columns} FROM " .
            static::$tableName .
            static::getFilters($filters);

        $result = MysqlDatabase::getResultFromQuery($sql);

        if ($result->num_rows === 0) {
            return null;
        } else {
            return $result;
        }
    }


    //where function
    private static function getFilters($filters)
    {
        $sql = '';

        if (count($filters) > 0) {
            $sql = " WHERE 1 = 1 ";
            foreach ($filters as $column => $value) {
                $sql .= " AND ${column} = " .
                    static::getFormatedValue($value);
            }
        }


        return $sql;
    }

    //formated value for string or number
    private static function getFormatedValue($value)
    {

        if (is_null($value)) {
            return null;
        } else if (gettype($value) == 'string') {
            return "'${value}'";
        } else {
            return $value;
        }
    }
}
