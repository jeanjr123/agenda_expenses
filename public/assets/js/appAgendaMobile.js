

let array_phones = [];
let array_mobilePhones = [];
let contPhones = 0;


//CREATE INPUT HIDDEN FOR SUBMIT PHONES
$(document).on("click", "#insertPhones", function () {
  contPhones++;

  let classPhone = "phone" + contPhones;
  let phone = $('#phone').val();
  let mobilePhone = $('#mobilePhone').val();

  array_phones.push(phone);
  array_mobilePhones.push(mobilePhone)

  $("#showPhones").append("<tr id=" + classPhone + "><td>" + phone + "</td><td>" + mobilePhone + "</td><td><a  class='btn btn-danger' onclick='removePhone(" + '"' + classPhone + '"' + ")'><i class='fa fa-trash'></i></a></td></tr>");



  $('#phoneSubmit').val(array_phones);
  $('#mobileSubmit').val(array_mobilePhones);

  $('#phone').val("");
  $('#mobilePhone').val("");

  return false;

});

function removePhone(idPhones) {

  $(`#${idPhones}`).remove();

}




//CREATE INPUT HIDDEN FOR SUBMIT ADRESSES

let array_streets = [];
let array_numbers = [];
let array_ceps = [];
let array_cities = [];
let array_districts = [];
let array_states = [];
let contAddress = 0;


$(document).on("click", "#insertAddress", function () {
  contAddress++;
  let classAddress = "address" + contAddress;
  let street = $('#street').val();
  let number = $('#number').val();
  let cep = $('#cep').val();
  let district = $('#district').val();
  let city = $('#city').val();
  let state = $('#state').val();


  array_streets.push(street);
  array_numbers.push(number);
  array_ceps.push(cep);
  array_districts.push(district);
  array_cities.push(city);
  array_states.push(state);

  $("#showAdresses").append("<tr id=" + classAddress + "><td>" + street + "</td><td>" + number + "</td><td>" + district + "</td><td>" + cep + "</td><td>" + city + "</td><td>" + state + "</td><td><a  class='btn btn-danger' onclick='removeAddress(" + '"' + classAddress + '"' + ")'><i class='fa fa-trash'></i></a></td></tr>");

  $('#streetSubmit').val(array_streets);
  $('#numberSubmit').val(array_numbers);
  $('#cepSubmit').val(array_ceps);
  $('#districtSubmit').val(array_districts);
  $('#citySubmit').val(array_cities);
  $('#stateSubmit').val(array_states);

  $('#street').val("");
  $('#number').val("");
  $('#cep').val("");
  $('#district').val("");
  $('#city').val("");
  $('#state').val("");

  return false;

})

function removeAddress(idAddress) {


  $(`#${idAddress}`).remove();

}




//MASKS
$("#phone").mask("(00) 0000-0000");
$("#mobilePhone").mask("(00) 00000-0000");
$("#cep").mask("00.000-000");

$(document).on("blur", "#cep", function () {

  var cep = $("#cep").val();

  $.ajax({
    url: "/apiCorreios.php",
    type: "POST",
    datatype: "json",
    data: "cep=" + cep,
    success: function (data) {
      var jsonData = JSON.parse(data);
      $('#street').val(jsonData.logradouro);

      $('#district').val(jsonData.bairro);

      $('#city').val(jsonData.localidade);

      $('#state').val(jsonData.uf);


    }

  });


})

//FUNCTION FOR DELETE PHONE NUMBERS

function deletePhones(idPhones) {

  swal({
    title: "Deseja realmente excluir o telefone?",
    text: "",
    icon: "warning",
    buttons: ["Não", "Sim"],
    dangerMode: true,
  })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({

          url: "cadPhones.php",
          type: "POST",
          datatype: "json",
          data: "idPhones=" + idPhones,

          success: function () {

            $(`#${idPhones}`).remove();

            swal("Telefone excluído com sucesso !", {
              icon: "success",
            });

          }

        });

      } else {
        null;
      }
    });
}

//FUNCTION FOR UPDATE PHONES

function updatePhones(idPhones, idUser, phone, mobile) {


  let phoneNumb = $(`#${phone}`).val();
  let mobileNumb = $(`#${mobile}`).val();


  $.ajax({

    url: "cadPhones.php",
    type: "POST",
    datatype: "json",
    data: "idPhonenumber=" + idPhones + "&idUser=" + idUser + "&mobilePhone=" + mobileNumb + "&phone=" + phoneNumb,

    success: function () {

      swal("Telefone atualizado com sucesso !", {
        icon: "success",
      });

    }

  });
}


//FUNCTION FOR DELETE ADRESSES
function deleteAdresses(idAddress, idAddressRow) {



  swal({
    title: "Deseja realmente excluir o endereço?",
    text: "",
    icon: "warning",
    buttons: ["Não", "Sim"],
    dangerMode: true,
  })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({

          url: "cadAddress.php",
          type: "POST",
          datatype: "json",
          data: "idAdresses=" + idAddress,

          success: function () {

            $(`#${idAddressRow}`).remove();

            swal("Endereço excluído com sucesso !", {
              icon: "success",
            });

          }

        });

      } else {
        null;
      }
    });
}

//FUNCTION FOR UPDATE ADRESSES
function updateAdresses(idAddress, idUser, street, number, district, cep, city, state) {

  let value_street = $(`#${street}`).val();
  let value_number = $(`#${number}`).val();
  let value_district = $(`#${district}`).val();
  let value_cep = $(`#${cep}`).val();
  let value_city = $(`#${city}`).val();
  let value_state = $(`#${state}`).val();

  $.ajax({

    url: "cadAddress.php",
    type: "POST",
    datatype: "json",
    data: "idAddress=" + idAddress + "&idUser=" + idUser + "&street=" + value_street + "&number=" + value_number +
      '&district=' + value_district + '&cep=' + value_cep + '&city=' + value_city +
      '&state=' + value_state + '&update=' + 1,

    success: function () {

      swal("Endereço atualizado com sucesso !", {
        icon: "success",
      });

    }

  });
}

//DELETE CATEGORY
function deleteCategory(idCategory) {


  swal({
    title: "Deseja realmente excluir a categoria?",
    text: "",
    icon: "warning",
    buttons: ['Não', 'Sim'],
    dangerMode: true,
  })
    .then((willDelete) => {
      if (willDelete) {

        window.location.href = "?delete=" + idCategory;

      } else {

        return null;
      }
    });

  return false;


}


//DELETE USER

function deleteUser(idUser) {


  swal({
    title: "Deseja realmente excluir o usuario?",
    text: "",
    icon: "warning",
    buttons: ['Não', 'Sim'],
    dangerMode: true,
  })
    .then((willDelete) => {
      if (willDelete) {

        window.location.href = "?delete=" + idUser;

      } else {

        return null;
      }
    });

  return false;


}


//CREATE TOKEN
$(document).on("click", "#createToken", function () {

  token = $('#token').val();

  if (token == "") {

    swal("Insira o token!", "", "error");

  } else {
    $.ajax({

      url: "cadToken.php",
      type: "POST",
      datatype: "json",
      data: "token=" + token,

      success: function () {
        swal("Token cadastrado com sucesso!",
          "", "success"
        );

        setTimeout(function () {
          window.location.href = "/cadToken.php";
        }, 2000);
      }
    });
  }

  return false;

});



//UPDATE TOKEN

$(document).on("click", "#updateToken", function () {

  idToken = $('#idToken').val();

  token = $('#token').val();


  if (token == "") {

    swal("Insira o token!", "", "error");

  } else {
    $.ajax({

      url: "cadToken.php",
      type: "POST",
      datatype: "json",
      data: "token=" + token + "&idToken=" + idToken,

      success: function () {
        swal("Token atualizado com sucesso!",
          "", "success"
        );

        setTimeout(function () {
          window.location.href = "/cadToken.php";
        }, 2000);
      }
    });
  }

  return false;

});









